// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "API.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class MYPROJECT20_API UAPI : public UObject
{
	GENERATED_BODY()

	static TArray <FString> Words;
	
public:
	UFUNCTION(BlueprintCallable)
	static FString GetRandomWord();
};
