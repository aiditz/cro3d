// Fill out your copyright notice in the Description page of Project Settings.

#include "API.h"
#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"

TArray <FString> InitWords() {
	TArray <FString> out;
	out.Add("Red");
	out.Add("Orange");
	out.Add("Yellow");

	return out;
}

TArray <FString> UAPI::Words = InitWords();

FString UAPI::GetRandomWord()
{
	int ArrayLength = UAPI::Words.Num();
	int Index = rand() % ArrayLength;

	return UAPI::Words[Index];
}